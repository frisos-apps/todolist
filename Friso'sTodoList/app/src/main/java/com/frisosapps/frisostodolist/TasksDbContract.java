package com.frisosapps.frisostodolist;

/**
 * Created by Friso on 16/6/24.
 */
public final class TasksDbContract {
    /**
     *
     */
    private TasksDbContract() {}

    /** */
    public final static String TASKS_TABLE = "tasks";
    /** */
    public final static String TASKS_KEY_ID = "id";
    /** */
    public final static String TASKS_KEY_DESCRIPTION = "description";
    /** */
    public final static String TASKS_KEY_IS_DONE = "is_done";
    /** */
    public final static String TASKS_KEY_DATE_EDITED = "date_edited";


    /** */
    public final static String TASKS_CREATE = "create table " + TASKS_TABLE + "(" +
            TASKS_KEY_ID + " integer primary key autoincrement, " +
            TASKS_KEY_DESCRIPTION + " text not null, " +
            TASKS_KEY_IS_DONE + " integer not null default 0, " +
            TASKS_KEY_DATE_EDITED + " integer not null);";
}
