package com.frisosapps.frisostodolist;

import java.util.Date;

/**
 * Created by Friso on 16/6/24.
 */
public class Task {
    private final int id;
    private String description;
    private boolean isDone;
    private final Date dateEdited;


    public Task(int id, String description, boolean isDone, Date dateEdited) {
        this.id = id;
        this.description = description;
        this.isDone = isDone;
        this.dateEdited = dateEdited;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public boolean isDone() {
        return isDone;
    }

    public Date getDateEdited() {
        return dateEdited;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    @Override
    public String toString() {
        return "Task {ID: " + id + ", Description: " + description + ", Is Done: " + isDone + ", Date: " + dateEdited.toString() + "}";
    }
}

