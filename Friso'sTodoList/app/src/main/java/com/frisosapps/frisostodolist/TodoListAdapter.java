package com.frisosapps.frisostodolist;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListAdapter;
import android.widget.TextView;

/**
 * Created by Friso on 16/6/25.
 */
public class TodoListAdapter extends CursorAdapter implements CompoundButton.OnCheckedChangeListener {

    private final TaskListFragment.OnListFragmentInteractionListener mListener;
    /**
     * Recommended constructor.
     *
     * @param context The context
     * @param c       The cursor from which to get the data.
     * @param flags   Flags used to determine the behavior of the adapter; may
     *                be any combination of {@link #FLAG_AUTO_REQUERY} and
     *                {@link #FLAG_REGISTER_CONTENT_OBSERVER}.
     */
    public TodoListAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        mListener = (TaskListFragment.OnListFragmentInteractionListener) context;
    }

    /**
     * @param position
     * @param convertView
     * @param parent
     * @see ListAdapter#getView(int, View, ViewGroup)
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return super.getView(position, convertView, parent);
    }

    /**
     * Makes a new view to hold the data pointed to by cursor.
     *
     * @param context Interface to application's global information
     * @param cursor  The cursor from which to get the data. The cursor is already
     *                moved to the correct position.
     * @param parent  The parent to which the new view is attached to
     * @return the newly created view.
     */
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater li = (LayoutInflater)getSystemService(context.LAYOUT_INFLATER_SERVICE);
        return li.inflate(R.layout.fragment_task, parent, false);
    }

    /**
     * Bind an existing view to the data pointed to by cursor
     *
     * @param view    Existing view, returned earlier by newView
     * @param context Interface to application's global information
     * @param cursor  The cursor from which to get the data. The cursor is already
     */
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        CheckBox cbTaskDone = (CheckBox)view.findViewById(R.id.cb_task_done);
        TextView tvDescription = (TextView)view.findViewById(R.id.tv_task_description);

        cbTaskDone.setChecked(cursor.getInt(cursor.getColumnIndex(TasksDbContract.TASKS_KEY_IS_DONE))==0? false:true);
        cbTaskDone.setOnCheckedChangeListener(this);
        tvDescription.setText(cursor.getString(cursor.getColumnIndex(TasksDbContract.TASKS_KEY_DESCRIPTION)));
    }

    /**
     * Called when the checked state of a compound button has changed.
     *
     * @param buttonView The compound button view whose state has changed.
     * @param isChecked  The new checked state of buttonView.
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }
}
