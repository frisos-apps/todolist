package com.frisosapps.frisostodolist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements TaskListFragment.OnListFragmentInteractionListener, TaskEditFragment.OnTaskEditFragmentInteractionListener {

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (findViewById(R.id.main_fragment_container) != null) {
            if (savedInstanceState != null) {
                return;
            }
            TaskListFragment listFrag = TaskListFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.main_fragment_container, listFrag).commit();
        }
    }

    /**
     *
     * @param taskDbId
     */
    @Override
    public void toEdit(int taskDbId){
        //use id to get cursor of single task

    }

    /**
     *
     */
    @Override
    public void saveTask() {

    }
}
