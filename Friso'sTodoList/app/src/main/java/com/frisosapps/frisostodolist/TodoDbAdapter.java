package com.frisosapps.frisostodolist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Date;

/**
 * Created by Friso on 16/6/24.
 */
public class TodoDbAdapter {
    /** */
    private static final String DB_NAME = "db_frisos_todo";
    /** */
    private static final int DB_VERSION = 1;
    /** */
    private static final String TAG = "TodoDbAdapter";

    /** */
    DatabaseHelper DBHelper;
    /** */
    SQLiteDatabase db;
    /** */
    final Context context;

    /**
     *
     * @param context
     */
    public TodoDbAdapter(Context context) {
        this.context = context;
        DBHelper = new DatabaseHelper(context);
    }

    /**
     *
     */
    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context)
        {
            super(context, DB_NAME, null, DB_VERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(TasksDbContract.TASKS_CREATE);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
//            db.execSQL("DROP TABLE IF EXISTS contacts");
            onCreate(db);
        }
    }

    /**
     *
     * @return
     * @throws SQLException
     */
    public TodoDbAdapter open() throws SQLException {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    /**
     *
     */
    public void close() {
        DBHelper.close();
    }

    /**
     *
     * @param task
     * @return
     */
    public long insertTask(Task task) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(TasksDbContract.TASKS_KEY_DESCRIPTION, task.getDescription());
        initialValues.put(TasksDbContract.TASKS_KEY_IS_DONE, task.isDone());
        initialValues.put(TasksDbContract.TASKS_KEY_DATE_EDITED, new Date().getTime());
        return db.insert(TasksDbContract.TASKS_TABLE, null, initialValues);
    }

    /**
     *
     * @param task
     * @return
     */
    public boolean deleteTask(Task task) {
        return db.delete(TasksDbContract.TASKS_TABLE, TasksDbContract.TASKS_KEY_ID + "=" + task.getId(), null) > 0;
    }

    /**
     *
     * @param id
     * @return
     */
    public Cursor getTask(int id) {
        return db.query(TasksDbContract.TASKS_TABLE, new String[] {TasksDbContract.TASKS_KEY_ID,
                        TasksDbContract.TASKS_KEY_DESCRIPTION, TasksDbContract.TASKS_KEY_IS_DONE,
                        TasksDbContract.TASKS_KEY_DATE_EDITED}, TasksDbContract.TASKS_KEY_ID + "=" + id, null, null, null, null);
    }

    /**
     *
     * @return
     */
    public Cursor getAllTasks() {
        return db.query(TasksDbContract.TASKS_TABLE, new String[] {TasksDbContract.TASKS_KEY_ID,
                TasksDbContract.TASKS_KEY_DESCRIPTION, TasksDbContract.TASKS_KEY_IS_DONE,
                TasksDbContract.TASKS_KEY_DATE_EDITED}, null, null, null, null,
                TasksDbContract.TASKS_KEY_IS_DONE + " ASC, " + TasksDbContract.TASKS_KEY_DATE_EDITED + " ASC");
    }

    /**
     *
     * @param task
     * @return
     */
    public boolean updateTask(Task task) {
        ContentValues args = new ContentValues();
        args.put(TasksDbContract.TASKS_KEY_DESCRIPTION, task.getDescription());
        args.put(TasksDbContract.TASKS_KEY_IS_DONE, task.isDone());
        args.put(TasksDbContract.TASKS_KEY_DATE_EDITED, new Date().getTime());
        return db.update(TasksDbContract.TASKS_TABLE, args, TasksDbContract.TASKS_KEY_ID + "=" + task.getId(), null) > 0;
    }
}
